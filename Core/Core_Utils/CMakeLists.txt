cmake_minimum_required(VERSION 3.8)
project(Core_Utils)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES src/TestUtils.cpp src/TestUtils.hpp)
add_library(Core_Utils SHARED ${SOURCE_FILES})