cmake_minimum_required(VERSION 3.8)
project(Core)

set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/Xaver/Libs)

# Core_Base
add_subdirectory(Core_Base)
add_subdirectory(Core_Utils)